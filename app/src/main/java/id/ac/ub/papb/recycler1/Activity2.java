package id.ac.ub.papb.recycler1;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.widget.TextView;

public class Activity2 extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_2);
        Bundle hasil = getIntent().getExtras();
        String nim = hasil.getString("nim");
        String nama = hasil.getString("nama");
        TextView tvnim = findViewById(R.id.tvNim2);
        TextView tvnama = findViewById(R.id.tvNama2);
        tvnim.setText(nim);
        tvnama.setText(nama);

    }
}